<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Validator;
use App\site;
class analyze extends BaseController
{
	public function addSite(Request $request)
	{

		$validator = Validator::make($request->all(), [
				'url' => 'required|active_url',
		]);

		if ($validator->fails()) {
			return $this->sendError('Validation Error.', $validator->errors()->first('url'));
			//return $validator->errors()->first();
		}


		$client = new Client();
		$res = $client->request('GET', $request->input('url'));
		$rawHeaders = $res->getHeaders();
		$score = 5;
		$secHeaders = array("Strict-Transport-Security","X-Frame-Options","X-Xss-Protection","X-Content-Type-Options","Content-Security-Policy");
		foreach($secHeaders as $secu){
			if(array_key_exists($secu,$rawHeaders)){
				$json["SecurityHeader"][$secu] = $rawHeaders[$secu][0]; 
				//$secu." : ".$rawHeaders[$secu][0]."<br/>";
			}else{
				$json["SecurityHeader"][$secu] = "null";
				$score = --$score;
			}
		}
		$json["score"] = $score;

		if(site::where('url',$request->input('url'))->doesntExist()){
			$insert = site::create([
					'url' => $request->input('url'),
					'rawHeaders' => json_encode($rawHeaders,JSON_UNESCAPED_SLASHES),
					'score' => $score,
			]);
		return $this->sendResponse($insert->toArray(), 'Site Security Header Was Analyzed');
		}else{
			return $this->sendError('Validation Error.', $validator->errors());
		}

		//return response()->json($json);

	}
	public function site($site){
		$check =[
			'url' => $site,
		];
		$validator = Validator::make($check,['url' =>'required|active_url']);

		if ($validator->fails()) {
			return $validator->errors()->first();
		}	
		$db = site::where('url',$site)->get()->toArray();
		print_r($db);

	}

	public function update($site){
		$check =[
			'url' => $site,
		];
		$validator = Validator::make($check,['url' =>'required|active_url']);

		if ($validator->fails()) {
			return $validator->errors()->first();
		}
	}
}
